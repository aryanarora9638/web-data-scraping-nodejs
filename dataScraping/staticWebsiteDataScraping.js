let cheerio        = require('cheerio')
let requestPromise = require('request-promise')

//Website to be scraped
const url = "https://en.wikipedia.org/wiki/List_of_Presidents_of_the_United_States"

requestPromise(url)
    .then( html  => {
        let wikiLinks = []

        let obj = cheerio('big > a', html)
        for (let key in obj){
            if(obj[key].attribs){
                wikiLinks.push(obj[key].attribs.href)
            }
        }
        return wikiLinks
    })
    .then(   async links => {
        //M-1
        let data = []
        let info
        for (let link of links) {
            info = await getAllBirthdayData(link)
            data.push(info)
        }
        return data

        //M-2
        // return await Promise.all(
        //     links.map( async link => {
        //         return await getAllBirthdayData(link)
        //     })
        // )
    })
    .then(finalData => {
        console.log(finalData)
    })
    .catch(err => {
        console.log("error 1")
    })


let getAllBirthdayData = (url) => {
     return requestPromise("https://en.wikipedia.org/" + url)
        .then( html => {
            return {
                name : cheerio('.firstHeading', html).text(),
                birthday : cheerio('.bday', html).text()
            }
        })
        .catch( err => {
            console.log("error 2")
        })
}