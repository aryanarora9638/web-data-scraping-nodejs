let puppeteer = require('puppeteer');
const url = 'https://www.reddit.com';
let cheerio = require('cheerio')

puppeteer
    .launch()
    .then(function(browser) {
        return browser.newPage();
    })
    .then(function(page) {
        return page.goto(url).then(function() {
            return page.content();
        });
    })
    .then(function(html) {
        cheerio('h3', html).each(function() {
            console.log(cheerio(this).text());
        });
    })
    .catch(function(err) {
        //handle error
    });