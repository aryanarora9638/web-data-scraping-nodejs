

let cheerio        = require('cheerio')
let puppeteer      = require('puppeteer-extra')
const pluginStealth = require("puppeteer-extra-plugin-stealth")
puppeteer.use(pluginStealth())
let userAgent      = require('random-useragent')
let baseURL      = "https://www.zillow.com"
let estateData     = {}
let extractedData  = {}
//https://www.zillow.com/homedetails/1790-W-62nd-Ave-Vancouver-BC-V6P-2G2/2083811542_zpid/
//https://www.zillow.com/homedetails/250-E-6th-Ave-702-Vancouver-BC-V5T-0B7/2083687964_zpid/
//https://www.zillow.com/homedetails/3436-Terra-Vita-Pl-61-Vancouver-BC-V5K-5H6/2083639308_zpid/

let getPageData = async (requestedURL) => {
    estateData = {}
    extractedData = {}

    let newURL = baseURL + "/homedetails/" + updateUrl(requestedURL)
    console.log(newURL)
    //let newURL = requestedURL
    await puppeteer
        .launch({headless : false})
        .then(async function (browser) {
            console.time("estate extraction test")
            let page =  await browser.newPage();
            await page.setRequestInterception(true)

            page.on('request', (req) => {
                //saves from loading all the images, css styles and font
                //thus saving memory and boosting performance
                if(req.resourceType() === 'image' || req.resourceType() === 'stylesheet' || req.resourceType() === 'font'){
                    req.abort()
                }
                else {
                    req.continue()
                }

            })

            let html
            await page.setUserAgent(userAgent.getRandom())
            await page.goto(newURL).then(async function () {
                html = await page.content();
                console.log("started")
                let priceObj = cheerio(".ds-value", html)
                let estateConfig = cheerio(".ds-bed-bath-living-area-container", html)
                let estateNameObj = cheerio(".ds-address-container", html)
                let estateSaleTypeObj = cheerio(".ds-chip-removable-content", html)
                let overviewInfoObj = cheerio(".ds-overview-stats", html)
                let descriptionObj = cheerio(".ds-overview-section", html)
                let openHouseObj = cheerio(".ds-openhouse-content.ds-overview-section", html)
                let agentObj = cheerio(".ds-overview-agent-card-container", html)
                let factsObj = cheerio(".ds-expandable-card-section-default-padding", html)
                let moreFactsObj = cheerio(".ds-expandable-card-collapsed", html)
                let priceHistoryObj = cheerio('.zsg-table.ds-price-and-tax-section-table', html)
                let nearbyHomesObj  = cheerio('.sc-229c99-1.BPgEU', html)
                //let nearbyHomesObj  = cheerio('.ds-mini-card-gallery-card-container', html)

                let neighborhoodScoresObj = cheerio('.zsg-list_inline.neighborhood-scores', html)
                //walk and Transit score pending api key


                let allImageObj = cheerio('.media-stream', html)


                extractedData.coverInfo = {}
                try {
                    //Main Estate Info
                    extractedData.coverInfo = {
                        ...extractedData.coverInfo,
                        estateName: estateNameObj[0].children[0].children[0].data + estateNameObj[0].children[1].children[0].data,
                        estatePrice: priceObj[0].children[0].data,
                        saleType: estateSaleTypeObj[0].children[0].children[0].children[1].data,
                        estateConfig: {
                            beds: estateConfig[0].children[0].children[0].children[0].data,
                            bath: estateConfig[0].children[1].children[0].children[0].data,
                            area: estateConfig[0].children[2].children[0].children[0].data
                        }
                    }
                } catch (e) {
                    console.log("Main Estate Info Error", e)
                }


                extractedData.estateOverView = {}
                try {
                    //OverView Estate Info

                    try {
                        //Three Pointers OverView Info
                        overviewInfoObj[0].children.forEach(child => {
                            extractedData.estateOverView = {
                                ...extractedData.estateOverView,
                                [child.children[0].children[0].data]: child.children[1].children[0].data
                            }
                        })
                        extractedData.estateOverView.discription = descriptionObj[1].children[0].children[0].children[0].data
                    } catch (e) {
                        console.log("Three Pointers OverView Info Error", e)
                    }

                    extractedData.estateOverView.openDay = {}
                    try {
                        //Open Day Info
                        if (openHouseObj[0].children[0].children[0].data === "Open Houses") {
                            let dayNum = 0
                            openHouseObj[0].children[1].children.forEach(child => {
                                dayNum++
                                extractedData.estateOverView.openDay = {
                                    ...extractedData.estateOverView.openDay,
                                    [dayNum]: {
                                        day: child.children[0].children[0].data,
                                        time: child.children[1].children[0].data
                                    }
                                }
                            })
                        }
                    } catch (e) {
                        console.log("Open Day Info error - ", e)
                    }

                    extractedData.estateOverView.agentInfo = {}
                    try {
                        //Agent Info
                        extractedData.estateOverView.agentInfo = {
                            imageLink: agentObj[0].children[0].children[1].children[0].children[0].attribs.src,
                            name: agentObj[0].children[0].children[1].children[1].children[0].children[0].children[0].data,
                            companyName: agentObj[0].children[0].children[1].children[1].children[1].children[0].children[0].data
                        }
                    } catch (e) {
                        console.log("Agent Info", e)
                    }

                    try {
                        extractedData.estateOverView.agentInfo.companyImageLink = agentObj[0].children[0].children[1].children[2].children[0].attribs.src
                    } catch (e) {
                        console.log("Company Image Link error", e)
                    }
                } catch (e) {
                    console.log("OverView Estate Info error - ", e)
                }

                extractedData.factsAndFeatures = {}
                try {
                    //Facts and Feature Info
                    factsObj[1].children[0].children.forEach(child => {
                        extractedData.factsAndFeatures = {
                            ...extractedData.factsAndFeatures,
                            [child.children[1].children[0].data]: child.children[2].children[0].data
                        }
                    })
                } catch (e) {
                    console.log("Facts and Feature info error", e)
                }
                //console.log(extractedData)


                try {
                    //More facts Info
                    moreFactsObj[1].children.forEach(child => {
                        let bigParent = child.children[0].children[0].children[0].data
                        estateData = {
                            ...estateData,
                            [bigParent]: null
                        }
                        child.children[0].children[1].children.forEach(subChild => {
                            let parent = subChild.children[0].children[0].data
                            estateData[bigParent] = {
                                ...estateData[bigParent],
                                [parent]: null
                            }
                            subChild.children[1].children[0].children.forEach(subsChild => {
                                //(subsChild)//tr - has 2 td (heading and value)
                                let heading = subsChild.children[0].children[0].data
                                let value = null
                                if (typeof subsChild.children[1].children[0].children[0].data === "undefined") {
                                    //value
                                    value = subsChild.children[1].children[0].children[0].children[0].data
                                } else {
                                    //value
                                    value = subsChild.children[1].children[0].children[0].data
                                }

                                estateData[bigParent][parent] = {
                                    ...estateData[bigParent][parent],
                                    [heading]: value.toString().length > 50 ? "No-Data" : value
                                }
                            })
                            //console.log(estateData)
                        })
                    })
                } catch (e) {
                    console.log("More Facts Info Error - ", e)
                }


                extractedData = {
                    ...extractedData,
                    moreFacts: estateData

                }

                extractedData.priceHistory = {}
                try{
                    //Price History Info
                    let historyNum = 0
                    priceHistoryObj[0].children[1].children.forEach(child => {
                        historyNum++
                        extractedData.priceHistory = {
                            ...extractedData.priceHistory,
                            [historyNum] : {
                                //could have placed another forEach loop here but it would just take more time,
                                // and since the fields are known to be specific like this we used definite paths
                                date  : child.children[0].children[0].data,
                                event : child.children[1].children[0].data,
                                price : child.children[2].children[0].data
                            }
                        }
                    })
                }
                catch (e) { console.log("Price History Info error - ", e)}

                //For getting the satellite view of the neighbourhood
                //use google api
                // - https://www.google.com/maps/@49.214749,-123.147209,390m/data=!3m1!1e3
                //uses longitude and latitude, with "data=!3m1!1e3" this specifying to see the satellite view than map view

                //Walk, bike and transit score pending for api key

                extractedData.nearbyHome = {}
                try{
                    //Nearby Home Info
                    let nearbyHomeLinks = []
                    let nearbyHomeNum = 0
                    try {
                        //Nearby Home Links Info
                        for(let key in nearbyHomesObj){
                            let url = nearbyHomesObj[key].children[0].attribs.href
                            nearbyHomeLinks.push(baseURL + url)
                            let imgLink = ""
                            let price = 0
                            let address = ""
                            let bed, bath, area = 0
                            nearbyHomesObj[key].children[0].children[0].children[0].children.forEach(child => {
                                if(child.attribs.class === "ds-mini-card-image"){
                                    imgLink = child.children[0].attribs.src
                                }
                                if(child.attribs.class === "ds-mini-card-data"){
                                    child.children.forEach(subChild => {
                                        if(subChild.attribs.class === "ds-mini-card-data-price-row"){
                                            price = subChild.children[0].children[0].data
                                        }
                                        if(subChild.attribs.class === "ds-mini-card-data-address-row ds-wrap-ellipsis"){
                                            address = subChild.children[0].children[0].data
                                        }
                                        if(subChild.attribs.class === "ds-mini-card-data-stats-row ds-wrap-ellipsis"){
                                            bed  = subChild.children[0].children[0].children[0].data
                                            bath = subChild.children[2].children[0].children[0].data
                                            area = subChild.children[4].children[0].children[0].data
                                        }
                                    })
                                }
                                else {
                                    return
                                }
                                nearbyHomeNum++
                                extractedData.nearbyHome = {
                                    ...extractedData.nearbyHome,
                                    [nearbyHomeNum] : {
                                        coverInfo : {
                                            estateName: address,
                                            estatePrice: price,
                                            saleType: "-",
                                            estateConfig: {
                                                beds: bed,
                                                bath: bath,
                                                area: area
                                            },
                                            estateLink : baseURL + url,
                                            estateCoverImgLink : imgLink
                                        }
                                    }
                                }
                            })
                        }
                    }
                    catch (e) { console.log("Nearby Home Links Info error - ", e)}
                }
                catch (e) { console.log("Nearby Home Info", e) }

                extractedData.gallery = {}
                try{
                    //All Image Links Info
                    let allImagesLink = []
                    allImageObj[0].children.forEach(child => {
                        if(child.children[0].name === "picture"){
                            allImagesLink.push(child.children[0].children[2].attribs.src)
                        }
                    })
                    extractedData.gallery = {
                        ...extractedData.gallery,
                        imageLink: allImagesLink
                    }
                }
                catch (e) { console.log("All Image Links Info Error  - ", e)}

                extractedData.neighborhoodScores = {}
                try{
                    let scoreType1 = neighborhoodScoresObj[0].children[0].children[0].children[0].children[1].data
                    let score1     = neighborhoodScoresObj[0].children[0].children[1].children[0].children[0].data
                    let comment1   = neighborhoodScoresObj[0].children[0].children[2].data
                    let scoreType2 = neighborhoodScoresObj[0].children[1].children[0].children[0].children[1].data
                    let score2     = neighborhoodScoresObj[0].children[1].children[1].children[0].children[0].data
                    let comment2   = neighborhoodScoresObj[0].children[1].children[2].data

                    extractedData.neighborhoodScores = {
                        ...extractedData.neighborhoodScores,
                        score1: {
                            type : scoreType1,
                            score : score1,
                            description : comment1
                        },
                        score2 : {
                            type : scoreType2,
                            score : score2,
                            description : comment2
                        }
                    }
                    console.log(scoreType1,score1,comment1, "    ", scoreType2, score2,comment2)
                }
                catch (e) { console.log("Neighborhood Scores Error - ", e)}


                //console.log(extractedData)
                console.timeEnd("estate extraction test")
                await page.close()
                await browser.close()
            });
        })
        // .then(async function (page) {
        //     //await page.setUserAgent(userAgent.getRandom())
        //     return page.goto(baseURL).then(function () {
        //         return page.content();
        //
        //     });
        // })
        // .then(async function (html) {
        //
        //
        // })
        .catch(function (err) {
            //handle error
        });
    return extractedData
}

let updateUrl = (url) => {
    let startIndex = url.toString().indexOf("[aryanSecretKey]")
    let keyLength = "[aryanSecretKey]".length
    let newUrl = url.substring(0, startIndex) + "/" + url.substring(startIndex+keyLength, url.length)
    return newUrl
}

module.exports.getPageData = getPageData


//Structure of extracted data - static
/*
* let extractedData = {
            estateName  : estateNameObj[0].children[0].children[0].children[0].children[0].data + estateNameObj[0].children[0].children[0].children[1].children[0].data,
            estatePrice : priceObj[0].children[0].data,
            saleType    : estateSaleTypeObj[0].children[0].children[0].children[1].data,
            estateConfig : {
                beds : estateConfig[0].children[0].children[0].children[0].data,
                bath : estateConfig[0].children[1].children[0].children[0].data,
                area : estateConfig[0].children[2].children[0].children[0].data
            },
            estateOverView : {
                daysOnWebsite : overviewInfoObj[0].children[0].children[1].children[0].data,
                totalViews    : overviewInfoObj[0].children[1].children[1].children[0].data,
                totalSaves    : overviewInfoObj[0].children[2].children[1].children[0].data,
                description   : descriptionObj[1].children[0].children[0].children[0].data,
                openDay       : {
                    date : openHouseObj[0].children[1].children[0].children[0].children[0].data,
                    time : openHouseObj[0].children[1].children[0].children[1].children[0].data
                },
                agentInfo : {
                    imageLink   : agentObj[0].children[0].children[1].children[0].children[0].attribs.src,
                    name        : agentObj[0].children[0].children[1].children[1].children[0].children[0].children[0].data,
                    companyName : agentObj[0].children[0].children[1].children[1].children[1].children[0].children[0].data,
                    companyImageLink : agentObj[0].children[0].children[1].children[2].children[0].attribs.src
                }
            },
            factsAndFeatures : {
                type         : factsObj[1].children[0].children[0].children[2].children[0].data,
                yearBuilt    : factsObj[1].children[0].children[1].children[2].children[0].data,
                heating      : factsObj[1].children[0].children[2].children[2].children[0].data,
                cooling      : factsObj[1].children[0].children[3].children[2].children[0].data,
                parking      : factsObj[1].children[0].children[4].children[2].children[0].data,
                lot          : factsObj[1].children[0].children[5].children[2].children[0].data,
                pricePerSqft : factsObj[1].children[0].children[6].children[2].children[0].data
            }
        }*/


/*
* let nearbyHomePage = await browser.newPage()
                    for(let link of nearbyHomeLinks){
                        // await nearbyHomePage.setRequestInterception(true)
                        // nearbyHomePage.on('request', (req) => {
                        //     if (req.resourceType() === 'image' || req.resourceType() === 'stylesheet' || req.resourceType() === 'font') {
                        //         req.abort()
                        //     } else {
                        //         req.continue()
                        //     }
                        // })

                        let htmlNearbyHome
                        await nearbyHomePage.setUserAgent(userAgent.getRandom())
                        await nearbyHomePage.goto(link).then(async function () {
                            htmlNearbyHome = await nearbyHomePage.content();
                            let priceObj2 = cheerio(".ds-value", htmlNearbyHome)
                            let estateConfig2 = cheerio(".ds-bed-bath-living-area-container", htmlNearbyHome)
                            let estateNameObj2 = cheerio(".ds-address-container", htmlNearbyHome)
                            let estateSaleTypeObj2 = cheerio(".ds-chip-removable-content", htmlNearbyHome)
                            nearbyHomeNum++
                            extractedData.nearbyHome = {
                                ...extractedData.nearbyHome,
                                [nearbyHomeNum] : {
                                    coverInfo : {
                                        estateName: estateNameObj2[0].children[0].children[0].data + estateNameObj2[0].children[1].children[0].data,
                                        estatePrice: priceObj2[0].children[0].data,
                                        saleType: estateSaleTypeObj2[0].children[0].children[0].children[1].data,
                                        estateConfig: {
                                            beds: estateConfig2[0].children[0].children[0].children[0].data,
                                            bath: estateConfig2[0].children[1].children[0].children[0].data,
                                            area: estateConfig2[0].children[2].children[0].children[0].data
                                        },
                                        estateLink : link
                                    }
                                }
                            }
                        })
                    }
                    await nearbyHomePage.close()*/