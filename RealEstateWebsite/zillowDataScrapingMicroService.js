let express = require('express')
let axios   = require('axios')
let bodyParser = require('body-parser')
let {getEstateData} = require('./zillowDataScraping')
let {getPageData}   = require('./estatePageDataScraping')

let app = express()

app.listen(3001, () => {
    console.log("Zillow Data Scraping Up and Running")
})

app.get('/', async (req, res) => {
    res.send("Home Page - Zillow Data Scraping MicroService")
})

app.get('/estates/:city/:province', async (req, res) => {
    let data = await getEstateData(req.params.city, req.params.province)
    res.header("Access-Control-Allow-Origin", "*");
    res.send(data)
    res.end()
})

app.get('/estates/:id', async (req, res) => {
    let pageData = await getPageData(req.params.id)
    res.header("Access-Control-Allow-Origin", "*");
    console.log(pageData)
    res.send(pageData)
    res.end()
})