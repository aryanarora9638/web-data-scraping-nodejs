

let cheerio        = require('cheerio')
let puppeteer      = require('puppeteer')
let userAgent      = require('random-useragent')
const baseURL      = "https://www.zillow.com/homedetails/3436-Terra-Vita-Pl-61-Vancouver-BC-V5K-5H6/2083639308_zpid/"
let estateData     = {}
//https://www.zillow.com/homedetails/1790-W-62nd-Ave-Vancouver-BC-V6P-2G2/2083811542_zpid/
//https://www.zillow.com/homedetails/250-E-6th-Ave-702-Vancouver-BC-V5T-0B7/2083687964_zpid/
//https://www.zillow.com/homedetails/3436-Terra-Vita-Pl-61-Vancouver-BC-V5K-5H6/2083639308_zpid/


puppeteer
    .launch()
    .then(async function(browser) {
        console.time("estate extraction test")
        let page = await browser.newPage();
        await page.setUserAgent(userAgent.getRandom())
        return page.goto(baseURL).then(async function() {
            let html = await page.content();
            console.log("started")
            let priceObj = cheerio(".ds-value", html)
            let estateConfig = cheerio(".ds-bed-bath-living-area-container", html)
            let estateNameObj = cheerio(".ds-price-change-address-row", html)
            let estateSaleTypeObj = cheerio(".ds-chip-removable-content", html)
            let overviewInfoObj = cheerio(".ds-overview-stats", html)
            let descriptionObj = cheerio(".ds-overview-section", html)
            let openHouseObj   = cheerio(".ds-openhouse-content.ds-overview-section", html)
            let agentObj = cheerio(".ds-overview-agent-card-container", html)
            let factsObj = cheerio(".ds-expandable-card-section-default-padding", html)
            let moreFactsObj = cheerio(".ds-expandable-card-collapsed", html)


            let extractedData = {}
            extractedData.coverInfo = {}
            try{
                //Main Estate Info
                extractedData.coverInfo = {
                    ...extractedData.coverInfo,
                    estateName  : estateNameObj[0].children[0].children[0].children[0].children[0].data + estateNameObj[0].children[0].children[0].children[1].children[0].data,
                    estatePrice : priceObj[0].children[0].data,
                    saleType    : estateSaleTypeObj[0].children[0].children[0].children[1].data,
                    estateConfig : {
                        beds : estateConfig[0].children[0].children[0].children[0].data,
                        bath : estateConfig[0].children[1].children[0].children[0].data,
                        area : estateConfig[0].children[2].children[0].children[0].data
                    }
                }
            }
            catch (e) { console.log("Main Estate Info Error", e) }



            extractedData.estateOverView = {}
            try {
                //OverView Estate Info

                try {
                    //Three Pointers OverView Info
                    overviewInfoObj[0].children.forEach(child => {
                        extractedData.estateOverView = {
                            ...extractedData.estateOverView,
                            [child.children[0].children[0].data] : child.children[1].children[0].data
                        }
                    })
                    extractedData.estateOverView.discription = descriptionObj[1].children[0].children[0].children[0].data
                }
                catch (e) { console.log("Three Pointers OverView Info Error", e) }

                extractedData.estateOverView.openDay = {}
                try{
                    //Open Day Info
                    if(openHouseObj[0].children[0].children[0].data === "Open Houses"){
                        let dayNum = 0
                        openHouseObj[0].children[1].children.forEach(child => {
                            dayNum++
                            extractedData.estateOverView.openDay = {
                                ...extractedData.estateOverView.openDay,
                                [dayNum] : {
                                    day  : child.children[0].children[0].data,
                                    time : child.children[1].children[0].data
                                }
                            }
                        })
                    }
                }
                catch (e) { console.log("Open Day Info error - ", e) }

                extractedData.estateOverView.agentInfo = {}
                try {
                    //Agent Info
                    extractedData.estateOverView.agentInfo = {
                        imageLink   : agentObj[0].children[0].children[1].children[0].children[0].attribs.src,
                        name        : agentObj[0].children[0].children[1].children[1].children[0].children[0].children[0].data,
                        companyName : agentObj[0].children[0].children[1].children[1].children[1].children[0].children[0].data
                    }
                }
                catch (e) { console.log("Agent Info", e)}

                try {extractedData.estateOverView.agentInfo.companyImageLink = agentObj[0].children[0].children[1].children[2].children[0].attribs.src}
                catch (e) {console.log("Company Image Link error", e)}
            }
            catch (e) {
                console.log("OverView Estate Info error - " , e)
            }

            extractedData.factsAndFeatures = {}
            try {
                //Facts and Feature Info
                factsObj[1].children[0].children.forEach(child => {
                    extractedData.factsAndFeatures = {
                        ...extractedData.factsAndFeatures,
                        [child.children[1].children[0].data] : child.children[2].children[0].data
                    }
                })
            }
            catch (e) { console.log("Facts and Feature info error", e) }
            console.log(extractedData)


            try {
                //More facts Info
                moreFactsObj[1].children.forEach(child => {
                    let bigParent = child.children[0].children[0].children[0].data
                    estateData = {
                        ...estateData,
                        [bigParent] : null
                    }
                    child.children[0].children[1].children.forEach(subChild => {
                        let parent = subChild.children[0].children[0].data
                        estateData[bigParent] = {
                            ...estateData[bigParent],
                            [parent] : null
                        }
                        subChild.children[1].children[0].children.forEach(subsChild => {
                            //(subsChild)//tr - has 2 td (heading and value)
                            let heading = subsChild.children[0].children[0].data
                            let value = null
                            if(typeof subsChild.children[1].children[0].children[0].data === "undefined"){
                                //value
                                value = subsChild.children[1].children[0].children[0].children[0].data
                            }
                            else {
                                //value
                                value = subsChild.children[1].children[0].children[0].data
                            }

                            estateData[bigParent][parent] =  {
                                ...estateData[bigParent][parent],
                                [heading] : value.toString().length>50 ? "No-Data" : value
                            }
                        })
                        //console.log(estateData)
                    })
                })
            }
            catch (e) { console.log("More Facts Info Error - " , e) }


            extractedData = {
                ...extractedData,
                moreFacts : estateData

            }
            console.log(extractedData)
            page.close()
            browser.close()
            console.timeEnd("estate extraction test")
        });
    })
    .catch(function(err) {
        //handle error
    });


//Structure of extracted data - static
/*
* let extractedData = {
            estateName  : estateNameObj[0].children[0].children[0].children[0].children[0].data + estateNameObj[0].children[0].children[0].children[1].children[0].data,
            estatePrice : priceObj[0].children[0].data,
            saleType    : estateSaleTypeObj[0].children[0].children[0].children[1].data,
            estateConfig : {
                beds : estateConfig[0].children[0].children[0].children[0].data,
                bath : estateConfig[0].children[1].children[0].children[0].data,
                area : estateConfig[0].children[2].children[0].children[0].data
            },
            estateOverView : {
                daysOnWebsite : overviewInfoObj[0].children[0].children[1].children[0].data,
                totalViews    : overviewInfoObj[0].children[1].children[1].children[0].data,
                totalSaves    : overviewInfoObj[0].children[2].children[1].children[0].data,
                description   : descriptionObj[1].children[0].children[0].children[0].data,
                openDay       : {
                    date : openHouseObj[0].children[1].children[0].children[0].children[0].data,
                    time : openHouseObj[0].children[1].children[0].children[1].children[0].data
                },
                agentInfo : {
                    imageLink   : agentObj[0].children[0].children[1].children[0].children[0].attribs.src,
                    name        : agentObj[0].children[0].children[1].children[1].children[0].children[0].children[0].data,
                    companyName : agentObj[0].children[0].children[1].children[1].children[1].children[0].children[0].data,
                    companyImageLink : agentObj[0].children[0].children[1].children[2].children[0].attribs.src
                }
            },
            factsAndFeatures : {
                type         : factsObj[1].children[0].children[0].children[2].children[0].data,
                yearBuilt    : factsObj[1].children[0].children[1].children[2].children[0].data,
                heating      : factsObj[1].children[0].children[2].children[2].children[0].data,
                cooling      : factsObj[1].children[0].children[3].children[2].children[0].data,
                parking      : factsObj[1].children[0].children[4].children[2].children[0].data,
                lot          : factsObj[1].children[0].children[5].children[2].children[0].data,
                pricePerSqft : factsObj[1].children[0].children[6].children[2].children[0].data
            }
        }*/