//Dynamic website


// let events = require('events');
// const emitter = new events.EventEmitter()
// emitter.setMaxListeners(0)
// or 0 to turn off the limit
// emitter.setMaxListeners(0)


let cheerio        = require('cheerio')
let puppeteer      = require('puppeteer-extra')
const pluginStealth = require("puppeteer-extra-plugin-stealth")
puppeteer.use(pluginStealth())
let userAgent      = require('random-useragent')
const baseURL      = "https://www.zillow.com/vancouver-bc"
let estateData     = []
let urlLinks       = []


let getEstateData = async () => {
    let url
    for (let pgNum = 1; pgNum <= 13; pgNum++) {
        if (pgNum === 1) {
            url = baseURL + "/"
        } else {
            url = baseURL + ("/" + pgNum + "_p")
        }
        urlLinks.push(url)
    }
    console.log(urlLinks)
    await searchWebsite()
    //return estateLinks
    //module.exports = estateLinks
}

let searchWebsite = async () => {
    await puppeteer
        .launch({headless: false})
        //, userDataDir : "~/Library/Application Support/Google/Chrome"
        .then(function (browser) {
            return browser.newPage();
        })
        .then(async function (page) {
            let html
            //await page.setUserAgent(userAgent.getRandom())
            for(let url of urlLinks){
                console.log(url)
                await page.goto(url).then(async function () {
                    console.log("on url -", url)
                    html = await page.content();
                    let obj = await cheerio('.list-card-link.list-card-info', html)
                    console.log(obj.length)
                    let imgObj = await cheerio(".list-card-link.list-card-img", html)
                    let geoLocation = await cheerio(".photo-cards.photo-cards_wow", html)
                    console.log(obj.length)
                    for (let key in obj) {
                        if (obj[key].attribs) {
                            try {
                                let geoStr = await geoLocation[0].children[0].children[0].children[0].data
                                let geoObj = await (JSON.parse(geoStr)["geo"])

                                let extractedInfo = {
                                    estateName : await obj[key].children[0].children[0].data,
                                    estatePrice : await obj[key].children[2].children[0].children[0].data,
                                    saleType : await obj[key].children[1].children[0].next.data,
                                    estateConfig : {
                                        beds :  await obj[key].children[2].children[1].children[0].children[0].data,
                                        bath :  await obj[key].children[2].children[1].children[1].children[0].data,
                                        area :  await obj[key].children[2].children[1].children[2].children[0].data
                                    },
                                    estateLocation : {
                                        longitude : await geoObj.longitude,
                                        latitude : await geoObj.latitude
                                    },
                                    estateLink : await obj[key].attribs.href,
                                    estateCoverImgLink : await imgObj[0].children[0].attribs.src
                                }
                                await estateData.push(extractedInfo)
                                console.log(extractedInfo.estateName)
                            }
                            catch (e) {
                                console.log("Estate Skipped - ", obj[key].children[0].children[0].data, obj[key].attribs.href)
                            }
                        }
                    }
                    console.log(estateData.length)
                });
            }
            //Now read the page

            console.log("total - ", estateData.length)
            page.close()
        })
        .catch(function (err) {
            console.log(err)
        });
}


getEstateData().then(() => console.log("finished", estateData.length))

