let cheerio        = require('cheerio')
let puppeteer      = require('puppeteer-extra')
const pluginStealth = require("puppeteer-extra-plugin-stealth")
puppeteer.use(pluginStealth())
let userAgent      = require('random-useragent')
const baseURL      = "https://www.zillow.com/homedetails/3426-Osler-St-Vancouver-BC-V6H-2W3/2083811603_zpid/"
let imageLink = ""


let getImage = async (url) => {
    await puppeteer
        .launch({headless: false})
        .then(async function (browser) {
            let page = await browser.newPage();

            await page.setRequestInterception(true)
            page.on('request', (req) => {
                if (req.resourceType() === 'image' || req.resourceType() === 'stylesheet' || req.resourceType() === 'font') {
                    req.abort()
                } else {
                    req.continue()
                }

            })
            await page.setViewport({width: 1001, height: 1001});
            let html
            await page.setUserAgent(userAgent.getRandom())
            await page.goto(url).then(async () => {
                html = await page.content()
                let allImageObj = cheerio('.media-stream', html)
                try {
                    imageLink = allImageObj[0].children[0].children[0].children[2].attribs.src
                    console.log(imageLink)
                } catch (e) {
                    console.log("error", e)
                }
            })
            await page.close()
            await browser.close()
        })
        .catch(function (err) {
            console.log(err)
        });
    return imageLink
}

//test
// getImage("https://www.zillow.com/homedetails/3426-Osler-St-Vancouver-BC-V6H-2W3/2083811603_zpid/").then(() => console.log("done"))
module.exports.getImage = getImage